# import field
import units
import prefix
import measuredunit

# We'll start by temperature

kelvin = units.Unit( 0, 1, (0, 0, 0, 0, 1, 0, 0))
celsius = kelvin + 273.15
fahrenheit = kelvin * 5 / 9 + 459.67

gram = units.Unit(0, 0.001, (0, 1, 0, 0, 0, 0, 0))
ton = gram * 10**6
pound = gram * 453.59237

val = 150

mu1 = measuredunit.MeasuredUnit(val, celsius)

mu2 = measuredunit.MeasuredUnit(val, gram, prefix.kilo)


far1 = mu1.convert_to(fahrenheit)

t1 = mu2.convert_to(pound)

print(val, ' celsius = ' , far1, ' fahrenheit', prefix.mega)

print(val, ' kg = ' , t1, ' pounds')

input('press enter to close')