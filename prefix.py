
class Prefix(object):
    multiplier = 1
    
    def __init__(self, mult):
        self.multiplier = mult
        
    def tell_multiplier(self):
        return self.multiplier

        
tera = Prefix(1000000000000)
giga = Prefix(1000000000)
mega = Prefix(1000000)
kilo = Prefix(1000)
hecto = Prefix(100)
deca = Prefix(10)
deci = Prefix(0.1)
centi = Prefix(0.01)
milli = Prefix(0.001)
micro = Prefix(0.000001)
nano = Prefix(0.000000001)
pico = Prefix(0.000000000001)
