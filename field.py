
class FieldList(object):
    list = []
    
    def add_instance(self, instance):
        self.list.append(instance)

field_list = FieldList()

class Field(object):
    
    name = ''
    unit_list = []
    si_unit = ''
    si_unit_exp = ''
    
    def __str__(self):
        descr_string = self.name + ' ' + str(self.unit_list)
        return descr_string
        
    def __init__(self, name): # , si_unit, si_unit_exp):
        self.name = name
        # self.si_unit = si_unit
        # self.si_unit_ext = si_unit_exp
        
        field_list.add_instance(self)
        
    def tell_si_unit(self):
        return self.si_unit
    
    def tell_si_unit_exp(self):
        return self.si_unit_exp
        
    def tell_name(self):
        return self.name
        
    def tell_unit_list(self):
        return self.unit_list
        
    def add_unit(self, new_unit):
        self.unit_list.append(new_unit)
        
    def get_unit_by_name(self, name):
        for u in self.unit_list:
            if u.tell_name() == name:
                return u
        print('sorry, nothing was found')
        return Null
    