import numbers

# (0,0,0,0,0,0,0)
# those are exponents for:
# (metre, kilogram, second, ampere, kelvin, candela, mole)
# (length, mass, time, electric current, temperature, luminous intensity, amount of substance)
# gram part is a bit weird but we'll check it later

class Unit(object):
    

    si_addition = 0
    si_mult = 1
    si_vector = (0,0,0,0,0,0,0)
    
    def __init__(self, si_addition, si_mult, si_vector):

        self.si_addition = si_addition
        self.si_mult = si_mult
        
        self.si_vector = si_vector
        
    def __str__(self):
        return self.abbreviation
        
    def __sub__(self, substraction):

        if isinstance(substraction, numbers.Number):
            new_unit = Unit(self.si_addition + substraction, self.si_mult, self.si_vector)
        else:
            raise TypeError('Unit substraction does not support anything but numbers')
        return new_unit
        
    def __add__(self, addition):

        if isinstance(addition, numbers.Number):
            new_unit = Unit(self.si_addition - addition, self.si_mult, self.si_vector)
        else:
            raise TypeError('Unit addition does not support anything but numbers')
        return new_unit
        
    def __mul__(self, mult):

        if isinstance(mult, numbers.Number):
            new_unit = Unit(self.si_addition, self.si_mult/mult, self.si_vector)
        elif isinstance(mult, Unit):
            mult_vector = mult.tell_si_vector()
            mult_divider = mult.tell_multiplier()
            new_vector = tuple(x-y for (x, y) in zip(self.si_vector, mult_vector))
            new_mult = self.si_mult/mult_divider
            new_unit = Unit(self.si_addition, new_mult, new_vector)
        else:
            raise TypeError('Unit division does not support anything but unit and numbers')
        return new_unit
        
    def __truediv__(self, dividor):

        if isinstance(dividor, numbers.Number):
            new_unit = Unit(self.si_addition, self.si_mult*dividor, self.si_vector)
        elif isinstance(dividor, Unit):
            mult_vector = dividor.tell_si_vector()
            mult_multiplier = dividor.tell_multiplier()
            new_vector = tuple(x+y for (x, y) in zip(self.si_vector, mult_vector))
            new_mult = mult_multiplier*self.si_mult
            new_unit = Unit(self.si_addition, new_mult, new_vector)
        else:
            raise TypeError('Unit multiplication does not support anything but unit and numbers')
        return new_unit
        
    def tell_addition(self):
        return self.si_addition

    def tell_multiplier(self):
        return self.si_mult
        
    def tell_si_vector(self):
        return self.si_vector
        
    
    
    