# import units

class MeasuredUnit(object):
    value = 1
    unit = ''
    prefix = ''
    
    def __init__(self, val, un, exp = ''):
        self.value = val
        self.unit = un
        self.prefix = exp
        
    def __str__(self):
        descr_string = str(self.value) + ' ' + self.prefix + self.unit
        return descr_string
        
    def convert_to(self, new_unit, new_prefix = ''):
    
        current_mult = self.unit.tell_multiplier()
        new_mult = new_unit.tell_multiplier()
        
        current_addition = self.unit.tell_addition()
        new_addition = new_unit.tell_addition()
        
        if self.prefix:
            current_pref = self.prefix.tell_multiplier()
        else:
            current_pref = 1
            
        if new_prefix:
            new_pref = new_pref.tell_multiplier()
        else:
            new_pref = 1
            
        # print((self.value/current_pref - current_addition)*new_mult/current_mult + new_addition)
        
        new_value = ((self.value*current_pref - current_addition)*new_mult/current_mult + new_addition)/new_pref
        return new_value
    
        